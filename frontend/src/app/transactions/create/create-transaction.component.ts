import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Transaction } from '../../api';
import { TransactionService } from '../transaction.service';

@Component({
  selector: 'app-create-transaction',
  templateUrl: './create-transaction.component.html'
})
export class CreateTransactionComponent {
  newTransaction: Transaction = {
    id: '',
    from: '',
    to: '',
    amount: 0,
  };

  constructor(private transactionService: TransactionService, private router: Router) { }

  submit(): void {
    this.transactionService
      .save(this.newTransaction)
      .subscribe(() => {
        this.router.navigate(['transactions']);
      });
  }
}
