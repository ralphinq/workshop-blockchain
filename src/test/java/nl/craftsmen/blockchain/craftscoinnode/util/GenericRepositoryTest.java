package nl.craftsmen.blockchain.craftscoinnode.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GenericRepositoryTest {

    private static final String THISNODE = "thistestnode:8080";
    private static final String THISNODE_OTHER = "thistestnodeother:8080";
    private static final String HI = "hi!";

    private InstanceInfo instanceInfo = mock(InstanceInfo.class);

    private GenericRepository genericRepository;


    @Before
    public void setup() {
        genericRepository = new GenericRepository(instanceInfo);
    }

    @Test
    public void loadingNotExistantFileProducesEmptyOptional() {
        when(instanceInfo.getNode()).thenReturn(THISNODE_OTHER);
        assertThat(genericRepository.load(DataClass.class)).isEmpty();
    }

    @Test
    public void peerListCanBeSavedAndLoaded() {
        when(instanceInfo.getNode()).thenReturn(THISNODE);

        DataClass dataClass = new DataClass();
        dataClass.text = HI;
        genericRepository.save(dataClass);
        Optional<DataClass> dataclassOptional = genericRepository.load(DataClass.class);
        assertThat(dataclassOptional).isPresent().map(d -> d.text).contains(HI);
    }

    @After
    public void cleanup() throws IOException {
        Path path = Paths.get(System.getProperty("user.dir"), "thistestnode8080-dataclass.json");
        Files.deleteIfExists(path);
    }

}