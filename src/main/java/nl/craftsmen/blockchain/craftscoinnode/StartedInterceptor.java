package nl.craftsmen.blockchain.craftscoinnode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StartedInterceptor implements HandlerInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartedInterceptor.class);
    private boolean open = false;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (!open) {
            throw new RuntimeException("no open for business yet!");
        } else {
            return true;
        }
    }

    void open() {
        this.open = true;
        LOGGER.info("Node is open for business.");
    }
}
