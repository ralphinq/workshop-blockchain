package nl.craftsmen.blockchain.craftscoinnode.blockchain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import nl.craftsmen.blockchain.craftscoinnode.transaction.Transaction;

import java.util.HashSet;
import java.util.Set;

@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class Block {

    //TODO add fields

    //default constructor needed for json deserialization
    @SuppressWarnings({"unused", "WeakerAccess"})
    public Block() {
    }

    //TODO add constructor

    //TODO add getters and setters

    //TODO add toString()

}
