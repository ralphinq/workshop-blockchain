package nl.craftsmen.blockchain.craftscoinnode.wallet;

import nl.craftsmen.blockchain.craftscoinnode.blockchain.BlockchainService;
import nl.craftsmen.blockchain.craftscoinnode.transaction.Transaction;
import nl.craftsmen.blockchain.craftscoinnode.transaction.TransactionPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class WalletService {

    private final BlockchainService blockchainService;
    private final TransactionPool transactionPool;

    @Autowired
    public WalletService(BlockchainService blockchainService, TransactionPool transactionPool) {
        this.blockchainService = blockchainService;
        this.transactionPool = transactionPool;
    }

    /**
     * Prodcues a wallet.
     * @param walletId the wallet id.
     * @return information about the wallet.
     */
    public Wallet getWallet(String walletId) {
        //TODO implement me
        return null;
    }

}
