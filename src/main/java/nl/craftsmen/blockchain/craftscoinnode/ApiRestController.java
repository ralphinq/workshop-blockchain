package nl.craftsmen.blockchain.craftscoinnode;

import nl.craftsmen.blockchain.craftscoinnode.blockchain.Block;
import nl.craftsmen.blockchain.craftscoinnode.blockchain.Blockchain;
import nl.craftsmen.blockchain.craftscoinnode.blockchain.BlockchainService;
import nl.craftsmen.blockchain.craftscoinnode.network.Network;
import nl.craftsmen.blockchain.craftscoinnode.transaction.Transaction;
import nl.craftsmen.blockchain.craftscoinnode.wallet.Wallet;
import nl.craftsmen.blockchain.craftscoinnode.wallet.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
public class ApiRestController {

    private final BlockchainService blockchainService;

    private final WalletService walletService;

    private final Network network;

    @Autowired
    public ApiRestController(BlockchainService blockchainService, WalletService walletService, Network network) {
        this.blockchainService = blockchainService;
        this.walletService = walletService;
        this.network = network;
    }

    /**
     * Creates a new transaction on this node.
     * @param transactionInputDto the new transaction
     * @return a string describing in which block the transaction will(probably) end up in
     */
    @PostMapping("/api/createtransaction")
    public String newTransaction(@RequestBody TransactionInputDto transactionInputDto) {
        //TODO implement me
        long l = blockchainService.createTransaction(transactionInputDto.getFrom(),transactionInputDto.getTo(),transactionInputDto.getAmount());
        return "{\"message\": \"new transaction will be added to block " + l + "\"}";
    }

    /**
     * Gets all transactions in the transaction pool.
     * @return all transactions in the transaction pool.
     */
    @GetMapping("/api/pendingtransactions")
    public Set<Transaction> getPendingTransactions() {
        return blockchainService.getPendingTransactions();
    }

    /**
     * Initiates mining a new block.
     * @return the block that was just mined.
     */
    @PostMapping("/api/mine")
    public Block mine() {
        return blockchainService.mine();
    }

    /**
     * Gets the blockchain of this node
     * @return the blockchain of this node
     */
    @GetMapping("/api/blockchain")
    public Blockchain getBlockchain() {
        return blockchainService.retrieveBlockChain();
    }


    /**
     * Returns the list of known peers for this node.
     * @return the list of known peers.
     */
    @GetMapping("/api/peers")
    public Set<String> getPeers() {
        return network.getPeers();
    }

    /**
     * Checks the validity of the blockchain of this node
     * @return true or false
     */
    @GetMapping("/api/valid")
    public boolean isValid() {
        return blockchainService.isValid();
    }

    /**
     * Returns wallet information of a certain user
     * @param walletId the wallet id
     * @return the balance, confirmed, and unconfirmed transactions.
     */
    @GetMapping("/api/wallet/{walletId}")
    public Wallet getWallet(@PathVariable String walletId) {
        return walletService.getWallet(walletId);
    }

}
