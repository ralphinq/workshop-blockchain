package nl.craftsmen.blockchain.craftscoinnode.network;

import nl.craftsmen.blockchain.craftscoinnode.blockchain.Block;
import nl.craftsmen.blockchain.craftscoinnode.blockchain.BlockchainService;
import nl.craftsmen.blockchain.craftscoinnode.network.Network;
import nl.craftsmen.blockchain.craftscoinnode.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

import static nl.craftsmen.blockchain.craftscoinnode.network.Network.API_ADDBLOCK;
import static nl.craftsmen.blockchain.craftscoinnode.network.Network.API_ADDTRANSACTION;
import static nl.craftsmen.blockchain.craftscoinnode.network.Network.API_REGISTERNODE;

@RestController
public class NetworkRestController {

    private final BlockchainService blockchainService;

    private final Network network;

    @Autowired
    public NetworkRestController(BlockchainService blockchainService, Network network) {
        this.blockchainService = blockchainService;
        this.network = network;
    }

    /**
     * Called when the network notifies this node of a new peer.
     *
     * @param peer the new peer
     * @return the list of known peers by this node (excluding the peer that just arrived)
     */
    @PostMapping(API_REGISTERNODE)
    public Set<String> registerPeer(@RequestBody String peer) {
        return network.registerNewPeer(peer);
    }

    /**
     * Called when the network notifies this node of a new block.
     *
     * @param peer  the peer that initiated the request.
     * @param block the new block.
     */
    @PostMapping(API_ADDBLOCK)
    public void newBlock(@RequestHeader(value = "peer", required = false) String peer, @RequestBody Block block) {
        blockchainService.newBlockReceived(block, peer);
    }

    /**
     * Processes a transaction received from a peer in the network.
     *
     * @param peer        the peer (optional)
     * @param transaction the received transaction
     */
    @PostMapping(API_ADDTRANSACTION)
    public void addTransaction(@RequestHeader(value = "peer", required = false) String peer, @RequestBody Transaction transaction) {
        blockchainService.newTransactionReceived(transaction, peer);
    }


}
